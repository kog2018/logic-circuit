//回路の種類ごとの情報
//今後なにか追加
var kairo_info = [
    //0, AND
    {
        img: new Image(),
        img_src: 'img/and.png'
    },
    //1, OR
    {
        img: new Image(),
        img_src: 'img/or.png'
    },
    //2,XOR
    {
        img: new Image(),
        img_src: 'img/xor.png'
    }
];
//↑のimgを設定する
for (let i = 0; i < kairo_info.length; i++) {
    kairo_info[i].img.src = kairo_info[i].img_src;
}

//jsにおけるクラスの定義のひとつ
var Kairo = function (position, type) {
    this.id = kairo.length;
    this.x = position.x;
    this.y = position.y;
    this.type = type;
    //端を求める
    this.left = position.x - 47;
    this.right = position.x + 47;
    this.bottom = position.y + 19;
    this.top = position.y - 19;
}

//いろんな変数を定義
var pr = Kairo.prototype,
    kairo = [];
    btn = document.getElementsByClassName('btn'),
    cnv = document.getElementsByTagName('canvas')[0];
    ctx = cnv.getContext('2d'),
    //マウスの座標(リアルタイムに更新ではない)
    click = {},
    //表示の中心となる座標、スクロールに使用
    center = {
        x: 0.0,
        y: 0.0
    },
    zoom = 1.0,
    selected = undefined,
    drag = false,
    down = false;

//回路を描画
pr.draw = function () {
    let p = reverse_position({
        x: this.left,
        y: this.top
    });
    if (p.right < 0 || cnv.clientWidth < p.left ||
        p.bottom < 0 || cnv.clientHeight < p.top)
        return;
    ctx.drawImage(kairo_info[this.type].img, p.x, p.y, 95 * zoom, 39 * zoom);
}

//回路の種類を置き換え
pr.replace = function (type) {
    let p = reverse_position({
        x: this.left,
        y: this.top
    });
    this.type = type;
    ctx.clearRect(p.x, p.y, 95 * zoom, 39 * zoom);
    //undefinedで置き換え(下の回路選択ボタンが何も押されてない状態)→削除
    if (type == undefined) {
        for (let i = this.id + 1; i < kairo.length; kairo[i++].id--);
        kairo.splice(this.id, 1);
        return;
    }
    ctx.drawImage(kairo_info[this.type].img, p.x, p.y, 95 * zoom, 39 * zoom);
}
function size_setting() {
    //初回読み込み時とウインドウリサイズ時に各種要素のサイズを設定
    cnv.style.height = (window.innerHeight - cnv.getBoundingClientRect().y)* 0.85 + 'px';
    //↓これをしないと描画がうまくいかない
    cnv.height = cnv.clientHeight;
    cnv.width = cnv.clientWidth;
    document.getElementById('cnv_height').style.height = cnv.clientHeight + 'px';
    document.getElementById('btns_wrapper').style.height = (window.innerHeight - cnv.getBoundingClientRect().y) * 0.1 + 'px';
    document.getElementById('btns_wrapper').style.marginTop = (window.innerHeight - cnv.getBoundingClientRect().y) * 0.03 + 'px';
    document.getElementById('btns_wrapper').style.marginLeft = 'auto';
    document.getElementById('btns_wrapper').style.marginRight = 'auto';
}

//ボタン押した時
function select_btn(e) {
    //要素のclassが3つある→押されたボタンが元々選択されていた→押されていない状態にする(画面上の話)
    if (e.classList[2]) {
        e.className = 'btn ' + e.classList[1];
        selected = undefined;
        return;
    }
    //何か他のボタンが選択されていたらそのボタンが押されていない状態にする
    if (selected != undefined)
        btn[selected].className = 'btn ' + btn[selected].classList[1];
    //選択されているボタンのclassには'selected'を追加して、選択されている見た目にする
    e.className += ' selected';
    //選択されているボタンを保存するグローバル変数selectedに、classの2つ目を保存する
    selected = Number(e.classList[1]);
}

//全部描画し直す
function draw_all() {
    ctx.clearRect(0, 0, cnv.width, cnv.height);
    for (let i = 0; i < kairo.length; i++)
        kairo[i].draw();
}


//マウスが重なっているかどうかチェックする関数、eは座標
function click_kairoCheck(e) {
    let clk_conv = convert_position(e);
    for (let i = 0; i < kairo.length; i++) {
        //マウスカーソルが回路の上にあった場合はその回路のインデックスを返す
        if (Math.abs(clk_conv.x - kairo[i].x) <= 47 && Math.abs(clk_conv.y - kairo[i].y) <= 19)
            return i;
        //回路の上にはないがそこに回路を置くと重なってしまう場合にはその回路のインデックスを-にして返す
        if (Math.abs(clk_conv.x - kairo[i].x) < 95 && Math.abs(clk_conv.y - kairo[i].y) < 39) {
            return -i;
        }

    }
    return undefined;
}

//キャンバス上の座標をシステム上の座標に変換(ズームとスクロールを実現するために、そのまま保存はしない)
function convert_position(e) {
    //eventListenerで受け取ったものが渡された場合にはcanvasの左上の座標を引く必要がある
    if (e.pageX != undefined && e.pageY != undefined)
        return {
            x: (e.pageX - cnv.getBoundingClientRect().x - (cnv.clientWidth / 2)) / zoom + center.x,
            y: (e.pageY - cnv.getBoundingClientRect().y - (cnv.clientHeight / 2)) / zoom + center.y
        };
    return {
        x: (e.x - (cnv.clientWidth / 2)) / zoom + center.x,
        y: (e.y - (cnv.clientHeight / 2)) / zoom + center.y
    };
}

//システム上の座標をキャンバス上の座標に変換
function reverse_position(e) {
    let rtn = {};
    rtn.x = (e.x - center.x) * zoom + cnv.clientWidth / 2;
    rtn.y = (e.y - center.y) * zoom + cnv.clientHeight / 2;
    return rtn;
}

//クリックされた時
function cnv_click() {
    //マウスと重なっている回路を求め保存する
    let on_kairo = click_kairoCheck(mouse);
    //どの回路とも重なってなければ新しく回路を置く
    if (on_kairo == undefined) {
        //回路選択がundefinedなら何もしない
        if (selected != undefined) {
            kairo.push(new Kairo(convert_position(mouse), selected));
            kairo[kairo.length - 1].draw();
        }
        return;
    }
    //重なっていた場合は置き換えor削除
    if (on_kairo >= 0)
        kairo[on_kairo].replace(selected);
    //click_kairoCheckの返り値がマイナス(=回路の上にはないがそこに回路を(ry 
    //の時のやつもそのうち実装?それとも特に何もしない?
}

//マウスが動いたとき
function cnv_mousemove(e) {
    //左ボタンがおされていない→ドラッグではないとき
    if (!down) {
        //回路に重なってたらマウスポインターを指にして、そうじゃないときはデフォ
        if (click_kairoCheck(e) >= 0)
            cnv.style.cursor = 'pointer';
        else
            cnv.style.cursor = 'default';
        return;
    }
    //今マウスが押されたやつはドラッグだったからクリックの関数呼ぶなよ〜変数をtrueにする
    drag = true;
    //centerの値を書き換えて再描画することでスクロールを実現
    //当たり前だけどあんまり気持ちのいいスクロールじゃない…
    center.x -= (e.pageX - cnv.getBoundingClientRect().x - mouse.x) / zoom;
    center.y -= (e.pageY - cnv.getBoundingClientRect().y - mouse.y) / zoom;
    draw_all();
    //前回呼び出し時との差で動かす量を決めているので、mouseの値を更新
    mouse = {
        x: e.pageX - cnv.getBoundingClientRect().x,
        y: e.pageY - cnv.getBoundingClientRect().y,
    }
}

//左ボタンが押された時
function cnv_down(e) {
    //左ボタンが押されてるで〜変数をtrueにする
    down = true;
    //ドラッグだから(ry　変数をfalseにしとく
    drag = false;
    //マウスの座標を更新
    mouse = {
        x: e.pageX - cnv.getBoundingClientRect().x,
        y: e.pageY - cnv.getBoundingClientRect().y
    }
}

//左ボタンが話された時
function cnv_up(e) {
    //押されてるで変数をfalseに
    down = false;
    //ドラッグじゃなければクリック関数を呼ぶ
    if (!drag)
        cnv_click();
}