//回路の種類ごとの情報
//今後なにか追加
var kairo_info = [
    //[0], AND
    {
        img: new Image(),
        img_src: 'img/and.png'
    },
    //[1], OR
    {
        img: new Image(),
        img_src: 'img/or.png'
    },
    //[2],XOR
    {
        img: new Image(),
        img_src: 'img/xor.png'
    }
];
//↑のimgを設定する
for (let i in kairo_info)
    kairo_info[i].img.src = kairo_info[i].img_src;

//jsにおけるクラスの定義のひとつ
var Kairo = function (position, type) {
    //this.idでthisのインデックスがわかる
    //回路を消すと反映にO(n)かかるのが難点(まあそんなに増えないだろうけど…)
    this.id = kairo.length;
    //中心座標x,y(こいつら使わなくね?)
    this.x = position.x;
    this.y = position.y;
    //回路の種類
    this.type = type;
    //上下左右の端の座標
    //サイズが固定なの修正しなきゃ
    this.left = position.x - 47;
    this.right = position.x + 47;
    this.bottom = position.y + 19;
    this.top = position.y - 19;
}

//いろんな変数を定義
var pr = Kairo.prototype,
    kairo = [],
    btn = document.getElementsByClassName('btn'),
    cnv = document.getElementsByTagName('canvas')[0],
    tooltip = document.getElementById('tooltip'),
    ctx = cnv.getContext('2d'),
    //マウスの座標とか(リアルタイムに更新ではない)
    mouse = {
        x: undefined,
        y: undefined,
        selected: undefined,
        status: undefined,
        ima_oita: undefined
    },
    y_btn = undefined,
    //表示の中心となる座標、スクロールに使用
    center = {
        x: 0.0,
        y: 0.0
    },
    //マウスクリック時にeventとして引数に渡される座標(ページ左上が始点)をキャンバスの左上始点に変換するときに使う値
    //canvasタグの座標+border
    cnv_offset = {
        x: cnv.getBoundingClientRect().x + 4,
        y: cnv.getBoundingClientRect().y + 4,
    },
    zoom = 1.0,
    dragging_kairo = undefined;

//回路を描画
pr.draw = function (drag) {
    let p = reverse_position(this);
    if (p.right < 0 || cnv.clientWidth < p.left ||
        p.bottom < 0 || (cnv.clientHeight < p.top))
        return;
    if (p.bottom > y_btn && !drag) {
        ctx.drawImage(kairo_info[this.type].img, 0, 0, 95, Math.round((y_btn - p.top) / zoom), Math.round(p.left), Math.round(p.top), Math.round(95 * zoom), Math.round(y_btn - p.top));
    }
    else
        ctx.drawImage(kairo_info[this.type].img, Math.round(p.left), Math.round(p.top), 95 * zoom, 39 * zoom);
};
//回路をキャンバスから消す
pr.erase = function () {
    let p = reverse_position(this);
    ctx.clearRect(Math.round(p.left), Math.round(p.top), 95, 39);
    let k = kairo_kairoKasanatteru(this);
    for (let i in k)
        kairo[k[i]].draw();
    draw_frame()
    draw_btnImg();
    draw_zoombtn();
};
//回路の存在を消す
pr.remove = function () {
    this.erase();
    for (let i = this.id+1; i < kairo.length; kairo[i++].id--);
    kairo.splice(this.id, 1);
}
//線を引く
function drawLine(sx, sy, ex, ey) {
    ctx.moveTo(sx, sy);
    ctx.lineTo(ex, ey);
}
//初回読み込み時とウインドウリサイズ時に各種要素のサイズを設定
function size_setting() {
    cnv.parentElement.style.height = Math.floor((window.innerHeight - cnv.getBoundingClientRect().y) * 0.98) + 'px';
    cnv.style.height = cnv.parentElement.style.height
    //↓これをしないと描画がうまくいかない
    cnv.height = cnv.clientHeight;
    cnv.width = cnv.clientWidth;
}

function draw_allKairo() {
    ctx.clearRect(0, 0, cnv.width, cnv.height);
    for (let i in kairo)
        kairo[i].draw();
}
//枠組みの線を引く
function draw_frame() {
    ctx.beginPath();
    ctx.lineWidth = 3;
    drawLine(0, y_btn, cnv.width, y_btn);
    for (let i = 1; i < 6; i++)
        drawLine(Math.round(cnv.width * i / 6), y_btn, Math.round(cnv.width * i / 6), cnv.height);
    ctx.stroke();
}
//下のボタンの画像を描画
function draw_btnImg() {
    let w = 95, h = 39;
    if (95 > cnv.width / 6) {
        w = Math.round(cnv.width / 6);
        h = Math.round(cnv.width * 39 / 95 / 6);
    }
    if (h > cnv.height - y_btn) {
        h = Math.round(cnv.height - y_btn);
        w = Math.round((cnv.height - y_btn) * 95 / 39);
    }
    for (let i = 0; i < 3; i++) {
        ctx.drawImage(kairo_info[i].img, Math.round(cnv.width * i / 6 + (cnv.width / 6 - w) / 2),
            Math.round(y_btn + (cnv.height - y_btn - h) / 2), w, h);
    }
}
//ズームボタンを描く
function draw_zoombtn() {
    ctx.beginPath();
    ctx.fillStyle = 'rgb(136,170,255)';
    ctx.lineWidth = 4;
    ctx.arc(cnv.width - 40, y_btn - 110, 30, 0, 2 * Math.PI);
    ctx.arc(cnv.width - 40, y_btn - 40, 30, 0, 2 * Math.PI);
    ctx.fill();
    ctx.beginPath();
    drawLine(cnv.width - 55, y_btn - 40, cnv.width - 25, y_btn - 40);
    drawLine(cnv.width - 55, y_btn - 110, cnv.width - 25, y_btn - 110);
    drawLine(cnv.width - 40, y_btn - 125, cnv.width - 40, y_btn - 95);
    ctx.stroke();
}
//全部描く
function draw_all() {
    draw_allKairo();
    draw_frame();
    draw_btnImg();
    draw_zoombtn();
}
//初期化
function init() {
    size_setting();
    y_btn = Math.round(cnv.height * 0.8);
    draw_all();
}

//kにtrueを指定すると、eはすでにcnv_offsetが引かれた値として扱う
function zoom_click(e,k) {
    let e2 = {
        x: e.x - cnv_offset.x,
        y: e.y - cnv_offset.y
    };
    //＋ボタン上にある時
    if (Math.hypot(e2.x - (cnv.width - 40), e2.y - (y_btn - 110)) <= 30)
        return 1;
    //‐ボタン上にある時
    if (Math.hypot(e2.x - (cnv.width - 40), e2.y - (y_btn - 40)) <= 30)
        return -1;
    return undefined;
} 

//マウスが重なっているかどうかチェックする関数、eは座標
function click_kairoKasanatteru(e) {
    let e2 = convert_position(e);
    let y_btn2 = convert_position({ x: 0, y: y_btn }).y;
    for (let i in kairo)
        if (kairo[i].left <= e2.x && e2.x <= kairo[i].right && kairo[i].top <= e2.y && e2.y <= (kairo[i].bottom < y_btn2 ? kairo[i].bottom : y_btn2))
            return i;
    return undefined;
}
//回路が回路と重なってるかどうかチェックする関数
function kairo_kairoKasanatteru(k) {
    let rtn = [];
    for (let i in kairo)
        if (kairo[i].left <= k.right && k.left <= kairo[i].right && kairo[i].top <= k.bottom && k.top <= kairo[i].bottom && kairo[i].id != k.id)
            rtn.push(i);
    return rtn;
}

//キャンバス上の座標をシステム上の座標に変換(ズームとスクロールを実現するために、そのまま保存はしない)
function convert_position(e) {
    //eventListenerで受け取ったものが渡された場合にはcanvasの左上の座標を引く必要がある
    if (e.pageX != undefined && e.pageY != undefined)
        return {
            x: (e.pageX - cnv_offset.x - (cnv.clientWidth / 2)) / zoom + center.x,
            y: (e.pageY - cnv_offset.y - (y_btn / 2)) / zoom + center.y
        };
    return {
        x: (e.x - (cnv.clientWidth / 2)) / zoom + center.x,
        y: (e.y - (y_btn / 2)) / zoom + center.y
    };
}

//システム上の座標をキャンバス上の座標に変換
function reverse_position(e) {
    if (e.draw)
        return {
            x: (e.x - center.x) * zoom + cnv.clientWidth / 2,
            y: (e.y - center.y) * zoom + y_btn / 2,
            left: (e.left - center.x) * zoom + cnv.clientWidth / 2,
            right: (e.right - center.x) * zoom + cnv.clientWidth / 2,
            top: (e.top - center.y) * zoom + y_btn / 2,
            bottom: (e.bottom - center.y) * zoom + y_btn / 2
        };
    return {
        x: (e.x - center.x) * zoom + cnv.clientWidth / 2,
        y: (e.y - center.y) * zoom + y_btn / 2
    };
}

//マウスが動いたとき
function cnv_move(e) {
    switch (mouse.status) {
        case 'put':
            if (dragging_kairo != undefined)
                dragging_kairo.erase();
            dragging_kairo = new Kairo(convert_position(e), mouse.selected);
            dragging_kairo.draw(true);
            break;
        case 'click':
            mouse.status = 'scr';
        case 'scr':
            cnv.style.cursor = 'pointer';
            tooltip.style.display = 'none';
            center.x += (mouse.x - e.x) / zoom;
            center.y += (mouse.y - e.y) / zoom;
            draw_all();
            mouse.x = e.x; 
            mouse.y = e.y;
            break;
        case undefined:
            //↓この関数の返り値を何度か使用するが全探査が含まれるので何度も呼びたくないので保存
            let kasa = click_kairoKasanatteru(e);
            if (kasa && mouse.ima_oita!=kasa) {
                cnv.style.cursor = 'pointer';
                tooltip.style.left = e.x - cnv_offset.x + 20 + 'px';
                tooltip.style.top = e.y - cnv_offset.y + 20 + 'px';
                tooltip.style.display = 'block';
            }
            else {
                if (zoom_click(e) || y_btn < e.y - cnv_offset.y)
                    cnv.style.cursor = 'pointer';
                else
                    cnv.style.cursor = 'default';
                tooltip.style.display = 'none';
                if (!kasa)
                    mouse.ima_oita = undefined;
            }
            break;
    }
}

//左ボタンが押された時
function cnv_down(e) {
    if (e.pageY - cnv_offset.y > y_btn) {
        //まだ3つしか回路を実装してないので、左の3つの領域のみ有効
        //全部の枠に回路を置いたら↓のif文は消す
        if (e.pageX - cnv_offset.x < cnv.width * 3 / 6) {
            mouse.status = 'put';
            mouse.selected = Math.floor((e.pageX - cnv_offset.x) * 6 / cnv.width);
            if (mouse.selected == 6)
                mouse.selected = 5;
        }
        return;
    }
    switch (zoom_click(e)) {
        case 1:
            mouse.status = 'plus';
            break;
        case -1:
            mouse.status = 'minus';
            break;
        case undefined:
            mouse.status = 'click';
    }
    mouse.x = e.x;
    mouse.y = e.y;
}   

//左ボタンが離された時
function body_up(e) {
    switch (mouse.status) {
        case 'put':
            if (reverse_position({ x: 0, y: dragging_kairo.top }).y > y_btn || kairo_kairoKasanatteru(dragging_kairo).length > 0) {
                dragging_kairo.erase();
                dragging_kairo = undefined;
                break;
            }
            kairo.push(dragging_kairo);
            mouse.ima_oita = kairo.length-1;
            if (reverse_position({ x: 0, y: dragging_kairo.bottom }).y > y_btn) {
                dragging_kairo.erase();
                dragging_kairo.draw();
            }
            dragging_kairo = undefined;
            break;
        case 'plus':
            if (zoom_click(e) == 1) {
                zoom += 0.25;
                draw_all();
            }
            break;
        case 'minus':
            if (zoom_click(e) == -1) {
                zoom -= 0.25;
                draw_all();
            }
            break;
        case 'click':
            //↓この関数の返り値を何度か使用するが全探査が含まれるので何度も呼びたくないので保存
            let kasa = click_kairoKasanatteru(e);
            if (kasa)
                kairo[kasa].remove();
            tooltip.style.display = 'none';
            break;
    }
    mouse.status = undefined;
}
//キャンバスからボタンが出た時
function cnv_out() {
    mouse.selected = undefined;
    cnv.style.cursor = 'default';
    if (mouse.status == 'put') {
        mouse.status = undefined;
        dragging_kairo.erase();
        dragging_kairo = undefined;
    }
}
//表示エリアからボタンが出た時
function body_out() {
    mouse.selected = undefined;
    mouse.status = undefined;
    cnv.style.cursor = 'default';
}